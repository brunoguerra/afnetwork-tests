//
//  OnlineRestWs.h
//  AFNetworking iOS Example
//
//  Created by System Administrator on 7/26/13.
//  Copyright (c) 2013 Gowalla. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPClient.h"

@interface OnlineRestWs : AFHTTPClient
+ (OnlineRestWs *)getInstance;
@end
