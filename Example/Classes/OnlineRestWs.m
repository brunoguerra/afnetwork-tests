//
//  OnlineRestWs.m
//  AFNetworking iOS Example
//
//  Created by System Administrator on 7/26/13.
//  Copyright (c) 2013 Gowalla. All rights reserved.
//

#import "OnlineRestWs.h"

#import "AFJSONRequestOperation.h"

static NSString * const ONLINE_URL = @"http://127.0.0.1:3002/";

@implementation OnlineRestWs
+ (OnlineRestWs *)getInstance {
    static OnlineRestWs *_instance;
    static dispatch_once_t _once_t;
    @synchronized(self) {
        dispatch_once(&_once_t, ^{
            _instance = [[OnlineRestWs alloc] initWithBaseURL:[NSURL URLWithString:ONLINE_URL]];
        });
    }
    
    return _instance;
}

- (id)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (!self) {
        return nil;
    }
    
    [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
    
    // Accept HTTP Header; see http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.1
	[self setDefaultHeader:@"Accept" value:@"application/json"];
    
    return self;
}


@end
