//
//  TestTableViewController.m
//  AFNetworking iOS Example
//
//  Created by System Administrator on 7/26/13.
//  Copyright (c) 2013 Gowalla. All rights reserved.
//

#import "TestTableViewController.h"
#import "TestTableViewControllerRemoteAjax.h"
#import "TestRemoteAjaxController.h"

@implementation TestTableViewController

NSMutableDictionary *menu;

-(void)loadView {
    [super loadView];
    menu = [[NSMutableDictionary alloc] init];
    [menu setObject:NSStringFromSelector(@selector(goToRemoteAjax:)) forKey:@"Remote Ajax"];
    [menu setObject:NSStringFromSelector(@selector(goToLogin:)) forKey:@"Login"];
    [menu setObject:NSStringFromSelector(@selector(goToRemoteAjax:)) forKey:@"Pics On-line"];
}

#pragma mark - navigation

- (id) goToRemoteAjax:(id)sender {
    TestRemoteAjaxController * _testRemoteAjax = [[TestRemoteAjaxController alloc] initWithNibName:@"TestRemoteAjaxController" bundle:nil];
    [self.navigationController pushViewController:_testRemoteAjax animated:true];
    return nil;
}

- (id) goToLogin:(id)sender {
    TestTableViewControllerRemoteAjax * _testTableViewRemoteAjax = [[TestTableViewControllerRemoteAjax alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:_testTableViewRemoteAjax animated:true];
    return nil;
}


#pragma mark - tableView

/**
 * None of the delegate methods are actually required, however you'll need to implement tableView:didSelectRowAtIndexPath: to handle touches on a table cell:
 */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    SEL sel = NSSelectorFromString([menu objectForKey:[[menu allKeys] objectAtIndex: indexPath.row]]);
    
    IMP methodImp = [self methodForSelector:sel];
    
    methodImp(self, sel, self);
}

/*
 * The following methods are required from the data source: tableView:numberOfRowsInSection: and tableView:cellForRowAtIndexPath:. If your table is a grouped table, you must also implement numberOfSectionsInTableView:.
 *
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"%d total menu items", menu.count);
    return [menu count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSLog(@"%@ - %d", CellIdentifier, indexPath.row);
    
    // Set the data for this cell:
    
    cell.textLabel.text = [[menu allKeys] objectAtIndex:indexPath.row];
    cell.imageView.image = [UIImage imageNamed:@"60x60_eng.png"];
    
    // set the accessory view:
    cell.accessoryType =  UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}


@end
