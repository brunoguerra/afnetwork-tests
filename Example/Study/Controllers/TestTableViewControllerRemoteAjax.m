//
//  TestTableViewController.m
//  AFNetworking iOS Example
//
//  Created by System Administrator on 7/26/13.
//  Copyright (c) 2013 Gowalla. All rights reserved.
//

#import "TestTableViewControllerRemoteAjax.h"

@interface TestTableViewControllerRemoteAjax ()
-(void)reload:(id)sender;
@end

@implementation TestTableViewControllerRemoteAjax

  __strong UIActivityIndicatorView *_activityIndicatorView;

-(void)reload:(id)sender {
    //[_activityIndicatorView startAnimating];
    //self.navigationItem.rightBarButtonItem.enabled = NO;
    
    /*[Post globalTimelinePostsWithBlock:^(NSArray *posts, NSError *error) {
        if (error) {
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:[error localizedDescription] delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil] show];
        } else {
            _posts = posts;
            [self.tableView reloadData];
        }
        
        [_activityIndicatorView stopAnimating];
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }];*/
}

-(void)loadView {
    [super loadView];
    _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    _activityIndicatorView.hidesWhenStopped = YES;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    [self reload:nil];
}


@end
