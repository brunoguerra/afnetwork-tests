//
//  TestRemoteAjaxController.m
//  AFNetworking iOS Example
//
//  Created by Bruno Guerra on 7/26/13.
//  Copyright (c) 2013 Gowalla. All rights reserved.
//

#import "TestRemoteAjaxController.h"

#import "RemoteItem.h"
#import "FacebookSDK/FBSession.h"

@interface TestRemoteAjaxController ()
-(void)reload:(id)sender;
@end

@implementation TestRemoteAjaxController {
@private
NSArray *_items;
__strong UIActivityIndicatorView *_activityIndicatorView;
    
}

@synthesize tableView = _tableView;


-(void)reload:(id)sender {
    [_activityIndicatorView startAnimating];
    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    [RemoteItem getResults:^(NSArray *items, NSError *error) {
     if (error) {
     [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:[error localizedDescription] delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil] show];
     } else {
     _items = items;
     [self.tableView reloadData];
     }
     
     [_activityIndicatorView stopAnimating];
     self.navigationItem.rightBarButtonItem.enabled = YES;
     }];
}


#pragma mark -

-(void)loadView {
    [super loadView];
    _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    _activityIndicatorView.hidesWhenStopped = YES;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_activityIndicatorView];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(reload:)];
    
    [self reload:nil];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [self setBtFacebook:nil];
    [super viewDidUnload];
}

#pragma mark - tableView


#pragma mark - UITableViewDataSource

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    int selectedRow = indexPath.row;
    NSLog(@"touch on row %d", selectedRow);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Set the data for this cell:
    
    RemoteItem * remoteItem = [_items objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%d", remoteItem.remoteItemID];
    cell.detailTextLabel.text = remoteItem.name;
    cell.imageView.image = [UIImage imageNamed:@"60x60_eng.png"];
    // set the accessory view:
    cell.accessoryType =  UITableViewCellAccessoryDetailDisclosureButton;
    
    return cell;
}

#pragma mark - toobar

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    NSLog([item title]);
    
    if ([item tag] == 1) {
        // If a user has *never* logged into your app, request one of
        // "email", "user_location", or "user_birthday". If you do not
        // pass in any permissions, "email" permissions will be automatically
        // requested for you. Other read permissions can also be included here.
        NSArray *permissions =
        [NSArray arrayWithObjects:@"email", nil];
        
        [FBSession openActiveSessionWithReadPermissions:permissions
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                                          /* handle success + failure in block */
                                      }];
    }
}


@end
