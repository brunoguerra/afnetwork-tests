//
//  TestRemoteAjaxController.h
//  AFNetworking iOS Example
//
//  Created by Bruno Guerra on 7/26/13.
//  Copyright (c) 2013 Gowalla. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestRemoteAjaxController : UIViewController {
    
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITabBarItem *btFacebook;

@end
